<?php
include ("config.php");
mysqli_set_charset('utf8');
$sql = "CREATE TABLE if not EXISTS users_info (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30) NOT NULL,
surname VARCHAR(30) NOT NULL,
email VARCHAR(50),
phone int(20),
gender VARCHAR(8),
birthdate DATE
)";

if ($conn->query($sql) === TRUE) {
    echo "Table created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

$conn->close();
?>
