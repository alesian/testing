<html>
<head>
    <title> Registration Form </title>
    <link href="style.css" rel="stylesheet">
</head>
<body>
<?php
session_start();
if ( isset( $_SESSION['addNewUserForm'] )) {
    echo $_SESSION['addNewUserForm']['error'];
    $name = htmlspecialchars ( $_SESSION['addNewUserForm']['name'] );
    $surname = htmlspecialchars ( $_SESSION['addNewUserForm']['surname'] );
    $email = htmlspecialchars ( $_SESSION['addNewUserForm']['email'] );
    $phone = htmlspecialchars ( $_SESSION['addNewUserForm']['phone'] );
    $gender = htmlspecialchars ( $_SESSION['addNewUserForm']['gender'] );
    $day = htmlspecialchars ( $_SESSION['addNewUserForm']['day'] );
    $month = htmlspecialchars ( $_SESSION['addNewUserForm']['month'] );
    $year = htmlspecialchars ( $_SESSION['addNewUserForm']['year'] );
    unset( $_SESSION['addNewUserForm'] );
}
else {
    $name     = '';
    $surname  = '';
    $email   = '';
    $phone      = '';
    $gender       = '';
    $day = '';
    $month = '';
    $year = '';
}
?>
<form action="save_user.php" method="post">
    <table class = "form">
        <tbody>
    <tr>
        <td>
        <label> Name: </label>
        </td>
        <td>
        <?php echo '<input type="text" name="Name" value="'.$name.'" />'."\n";  ?>
        </td>
    </tr>
    <tr>
        <td>
        <label> Surname: </label>
        </td>
        <td>
            <?php echo '<input type="text" name="Surname" value="'.$surname.'" />'."\n";  ?>
        </td>
    </tr>
    <tr>
        <td><label> Email: </label>  </td>
       <td>   <?php echo '<input type="text" name="Email" value="'.$email.'" />'."\n";  ?> </td>
    </tr>
    <tr>
        <td><label> Phone: </label> </td>
        <td><?php echo '<input type="text" name="Phone" value="'.$phone.'" />'."\n";  ?> </td>
    </tr>
    <tr>
        <td> <label> Gender: </label> </td>
        <td> <input type="radio" name="gender" value="male "  <?php if ($gender == 'male') { echo 'checked'; } ?>/> Male
        <input type="radio" name="gender" value="female" <?php if ($gender == 'female') { echo 'checked'; } ?> /> Female </td>
    </tr>
    <tr>
        <td> <label>Birthday:</label> </td>
        <td> <select name = "day">
           <?php
            $i = 1;
            while ($i<=31)
            {
                if ($day!=$i) {
                    echo "<option value='" . $i . "' >$i</option>";
                }
                else {
                    echo "<option value='" . $i . "' selected >$i</option>";
                }
                $i++;
            }
            ?>
        </select>
        <select name="month">
            <option value="01">January</option>
            <option value="02" <?php if ($month == '02') { echo 'selected'; }?> >February</option>
            <option value="03" <?php if ($month == '03') { echo 'selected'; }?> >March</option>
            <option value="04" <?php if ($month == '04') { echo 'selected'; }?> >April</option>
            <option value="05" <?php if ($month == '05') { echo 'selected'; }?>>May</option>
            <option value="06" <?php if ($month == '06') { echo 'selected'; }?>>June</option>
            <option value="07" <?php if ($month == '07') { echo 'selected'; }?>>July</option>
            <option value="08" <?php if ($month == '08') { echo 'selected'; }?>>August</option>
            <option value="09" <?php if ($month == '09') { echo 'selected'; }?>>September</option>
            <option value="10" <?php if ($month == '10') { echo 'selected'; }?>>October</option>
            <option value="11" <?php if ($month == '11') { echo 'selected'; }?>>November</option>
            <option value="12" <?php if ($month == '12') { echo 'selected'; }?>>December</option>
        </select>
        <select name = "year">
            <?php
            $j = 1990;
            while ($j<2013)
            {
                if ($year!=$j) {
                    echo "<option value='" . $j . "' >$j</option>";
                }
                else {
                    echo "<option value='" . $j . "' selected >$j</option>";
                }
                $j++;
            }
            ?>
        </select> </td>
    </tr>
    </tbody>
        </table>
    <div  class = "registration">
        <input class="button" type="submit" name="submit" value="Registration">
    </div>
</form>
</body>
</html>
